const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const WebpackManifestPlugin = require('webpack-yam-plugin');
const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const { VueLoaderPlugin } = require('vue-loader');

const PROD = process.env.NODE_ENV === 'production';
const SOURCE_MAPS = +(process.env.SOURCE_MAPS || 0);
const NO_MANIFEST = +(process.env.NO_MANIFEST || 0);

console.log(PROD ? 'PRODUCTION BUILD' : 'DEVELOPMENT BUILD');

function compact(array) {
  return [...array].filter(n => !!n);
}

module.exports = {
  context: __dirname,
  mode: PROD ? 'production' : 'development',
  entry: {
    admin: './bundles/admin',
    donate: './bundles/donate',
    hosting_station: './bundles/hosting_station',
  },
  output: {
    filename: PROD ? 'tracker-[name]-[hash].js' : 'tracker-[name].js',
    pathinfo: true,
    path: path.join(__dirname, 'tracker/static/gen'),
    publicPath: '/static/gen/',
  },
  stats: 'minimal',
  module: {
    rules: [
      {
        test: /\.[jt]sx?$/,
        exclude: /node_modules/,
        use: compact([!PROD && 'react-hot-loader/webpack', 'babel-loader']),
      },
      {
        test: /\.vue$/,
        use: 'vue-loader',
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: !PROD,
            },
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
              modules: {
                mode: 'local',
                localIdentName: '[local]--[hash:base64:10]',
              },
            },
          },
          'postcss-loader',
        ],
      },
      {
        test: /\.(png|jpg|svg|ttf|eot|svg|woff2?)/,
        use: 'file-loader',
      },
    ],
  },
  node: {
    fs: 'empty',
  },
  resolve: {
    alias: {
      ui: path.join(__dirname, 'bundles'),
      ...(PROD ? {} : { 'react-dom': '@hot-loader/react-dom' }),
    },
    extensions: ['.js', '.ts', '.tsx'],
  },
  optimization: {
    splitChunks: {
      chunks: 'async',
    },
    minimizer: [
      new TerserPlugin({
        cache: true,
        parallel: true,
        sourceMap: true,
        terserOptions: {
          output: {
            comments: /@license/i,
          },
        },
      }),
    ],
  },
  devServer: {
    disableHostCheck: true,
    proxy: {
      context: "!/sockjs-node/**",
      target: process.env.TRACKER_HOST || 'http://localhost:8000/',
      ws: true,
    },
  },
  plugins: [
    new VueLoaderPlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(),
    !NO_MANIFEST &&
      new WebpackManifestPlugin({
        manifestPath: path.join(__dirname, 'tracker/ui-tracker.manifest.json'),
          outputRoot: path.join(__dirname, 'tracker/static'),
      }),
    new MiniCssExtractPlugin({
      filename: PROD ? 'tracker-[name]-[hash].css' : 'tracker-[name].css',
      chunkFilename: PROD ? '[id].[hash].css' : '[id].css',
      ignoreOrder: false,
    }),
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'development',
    }),
    new webpack.DefinePlugin({
      __VUE_OPTIONS_API__: 'true',
      __VUE_PROD_DEVTOOLS__: 'false'
    })
  ],
  devtool: SOURCE_MAPS ? (PROD ? 'source-map' : 'eval-source-map') : false,
};
