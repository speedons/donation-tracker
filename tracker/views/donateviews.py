import datetime
import json
import logging
import random
import traceback
import post_office.mail
import pytz
import os

from decimal import Decimal
from gettext import gettext as _

from django.conf import settings
from django.db import transaction
from django.utils import translation
from django.db.models import F
from django.http import HttpResponse, Http404
from django.urls import reverse
from django.middleware import csrf
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from paypal.standard.forms import PayPalPaymentsForm

from tracker import forms, models, eventutil, viewutil, paypalutil, search_filters
from webpack_manifest import webpack_manifest

from tracker.decorators import no_querystring
from tracker.models import Bid

from ..serializers import DecimalEncoder
from .common import tracker_response
from .public import get_bid_info

__all__ = [
    'paypal_cancel',
    'paypal_return',
    'donate',
    'ipn',
]

logger = logging.getLogger(__name__)


@csrf_exempt
def paypal_cancel(request, event):
    return tracker_response(request, 'tracker/paypal_cancel.html', {
        'event': viewutil.get_event(event),
    })


@csrf_exempt
def paypal_return(request, event):
    return tracker_response(request, 'tracker/paypal_return.html', {
        'event': viewutil.get_event(event),
    })


def process_form(request, event):
    bidsFormPrefix = 'bidsform'
    if request.method == 'POST':
        commentform = forms.DonationEntryForm(event=event, data=request.POST)
        if commentform.is_valid():
            bidsform = forms.DonationBidFormSet(
                amount=commentform.cleaned_data['amount'],
                data=request.POST,
                prefix=bidsFormPrefix,
            )
            if bidsform.is_valid():
                with transaction.atomic():
                    donation = models.Donation(
                        amount=commentform.cleaned_data['amount'],
                        timereceived=pytz.utc.localize(datetime.datetime.utcnow()),
                        commentlanguage=translation.get_language_from_request(request),
                        domain='PAYPAL',
                        domainId=str(random.getrandbits(128)),
                        event=event,
                    )
                    if commentform.cleaned_data['comment']:
                        donation.comment = commentform.cleaned_data['comment']
                        donation.commentstate = 'PENDING'
                    donation.requestedvisibility = commentform.cleaned_data[
                        'requestedvisibility'
                    ]
                    donation.requestedalias = commentform.cleaned_data['requestedalias']
                    donation.requestedemail = commentform.cleaned_data['requestedemail']
                    donation.requested_receipt = commentform.cleaned_data['requested_receipt']
                    donation.currency = event.paypalcurrency
                    donation.save()
                    for bidform in bidsform:
                        if (
                            'bid' in bidform.cleaned_data
                            and bidform.cleaned_data['bid']
                        ):
                            bid = bidform.cleaned_data['bid']
                            if bid.allowuseroptions:
                                # unfortunately, you can't use get_or_create when using a non-atomic transaction
                                # this does technically introduce a race condition, I'm just going to hope that two people don't
                                # suggest the same option at the exact same time
                                # also, I want to do case-insensitive comparison on the name
                                try:
                                    bid = models.Bid.objects.get(
                                        event=bid.event,
                                        speedrun=bid.speedrun,
                                        name__iexact=bidform.cleaned_data[
                                            'customoptionname'
                                        ],
                                        parent=bid,
                                    )
                                except models.Bid.DoesNotExist:
                                    bid = models.Bid.objects.create(
                                        event=bid.event,
                                        speedrun=bid.speedrun,
                                        name=bidform.cleaned_data['customoptionname'],
                                        parent=bid,
                                        state='PENDING',
                                        istarget=True,
                                    )
                            donation.bids.add(
                                models.DonationBid(
                                    bid=bid,
                                    amount=Decimal(bidform.cleaned_data['amount']),
                                ),
                                bulk=False,
                            )
                    donation.full_clean()
                    donation.save()

                paypal_dict = {
                    'amount': str(donation.amount),
                    'cmd': '_donations',
                    'business': donation.event.paypalemail,
                    'image_url': donation.event.paypalimgurl,
                    'item_name': donation.event.receivername,
                    'notify_url': request.build_absolute_uri(reverse('tracker:ipn')),
                    'return': request.build_absolute_uri(
                        reverse('tracker:event:paypal_return', args=(donation.event.short,))
                    ),
                    'cancel_return': request.build_absolute_uri(
                        reverse('tracker:event:paypal_cancel', args=(donation.event.short,))
                    ),
                    'custom': str(donation.id) + ':' + donation.domainId,
                    'currency_code': donation.event.paypalcurrency,
                    'no_shipping': 0,
                }
                # Create the form instance
                form = PayPalPaymentsForm(button_type='donate', initial=paypal_dict)
                context = {'event': donation.event, 'form': form}
                return (
                    tracker_response(
                        request, 'tracker/paypal_redirect.html', context
                    ),
                    None,
                )
        else:
            bidsform = forms.DonationBidFormSet(
                amount=Decimal('0.00'), data=request.POST, prefix=bidsFormPrefix
            )
            bidsform.is_valid()
    else:
        commentform = forms.DonationEntryForm(event=event)
        bidsform = forms.DonationBidFormSet(
            amount=Decimal('0.00'), prefix=bidsFormPrefix
        )
    return commentform, bidsform


@csrf_protect
@no_querystring
def donate(request, event):
    bundle = webpack_manifest.load(
        os.path.abspath(
            os.path.join(os.path.dirname(__file__), '../ui-tracker.manifest.json')
        ),
        settings.STATIC_URL,
        debug=settings.DEBUG,
        timeout=60,
        read_retry=None,
    )

    event = viewutil.get_event(event)

    if event.locked:
        raise Http404

    if not event.allow_donations:
        return tracker_response(
            request,
            'tracker/donate.html',
            {
                'event': event,
            },
        )

    commentform, bidsform = process_form(request, event)

    if not bidsform:
        return commentform

    def event_info(event):
        result = {
            'id': event.id,
            'name': event.name,
            'receiver_name': event.receivername,
            'paypal_currency': event.paypalcurrency,
            'minimum_donation': event.minimumdonation,
            'donation_notes': event.donation_notes,
        }

        return result

    def bid_info(bid):
        result = {
            'id': bid.id,
            'parent': bid.parent_id,
            'name': bid.name,
            'description': bid.description,
            'short_description': bid.shortdescription,
            'total': bid.total,
            'goal': bid.goal,
        }

        if bid.speedrun:
            result['speedrun'] = {
                'id': bid.speedrun.id,
                'name': bid.speedrun.name,
                'category': bid.speedrun.category,
            }

        if bid.allowuseroptions:
            result['allow_user_options'] = True
            result['option_max_length'] = bid.option_max_length

        return result

    def prize_info(prize):
        result = {
            'id': prize.id,
            'name': prize.name,
            'description': prize.description,
            'minimumbid': prize.minimumbid,
            'maximumbid': prize.maximumbid,
            'keyword': prize.keyword,
            'sumdonations': prize.sumdonations,
            'url': prize.get_absolute_url(),
        }

        return result

    bids = search_filters.run_model_query('allbids', {'state': 'OPENED', 'event': event.id})
    prizes = search_filters.run_model_query('prize', {'feed': 'current', 'event': event.id})

    bidsArray = [bid_info(bid) for bid in bids]
    prizesArray = [prize_info(prize) for prize in prizes]

    def to_json(value):
        return value.id if hasattr(value, 'id') else value

    initialData = {
        k: to_json(commentform.cleaned_data[k])
        for k, v in commentform.fields.items()
        if commentform.is_bound and k in commentform.cleaned_data
    }

    initialData['bids'] = [
        {
            k: to_json(form.cleaned_data[k])
            for k, v in form.fields.items()
            if k in form.cleaned_data
        }
        for form in bidsform.forms
        if form.is_bound
    ]

    initialErrors = json.loads(commentform.errors.as_json())
    initialErrors['bids'] = bidsform.errors

    return tracker_response(
        request,
        'tracker/donate.html',
        {
            'event': event,
            'bundle': bundle.donate,
            'props': json.dumps(
                {
                    'locale': translation.get_language_from_request(request),
                    'csrfToken': csrf.get_token(request),
                    'availablePrizes': prizesArray,
                    'availableBids': bidsArray,
                    'event': event_info(event),
                    'initialData': initialData,
                    'initialErrors': initialErrors,
                },
                ensure_ascii=False,
                cls=DecimalEncoder,
            ),
        },
    )


@csrf_exempt
@never_cache
def ipn(request):
    ipnObj = None

    if request.method == 'GET' or len(request.POST) == 0:
        return tracker_response(request, '404.html', {})

    try:
        ipnObj = paypalutil.create_ipn(request)
        ipnObj.save()

        donation = paypalutil.initialize_paypal_donation(ipnObj)
        donation.save()

        if donation.transactionstate == 'PENDING':
            reasonExplanation, ourFault = paypalutil.get_pending_reason_details(
                ipnObj.pending_reason
            )
            if donation.event.pendingdonationemailtemplate:
                formatContext = {
                    'event': donation.event,
                    'donation': donation,
                    'donor': donation.donor,
                    'pending_reason': ipnObj.pending_reason,
                    'reason_info': reasonExplanation if not ourFault else '',
                }
                post_office.mail.send(
                    recipients=[donation.donor.email],
                    sender=donation.event.donationemailsender,
                    template=donation.event.pendingdonationemailtemplate,
                    context=formatContext,
                )
            # some pending reasons can be a problem with the receiver account, we should keep track of them
            if ourFault:
                paypalutil.log_ipn(ipnObj, 'Unhandled pending error')
        elif donation.transactionstate == 'COMPLETED':
            if donation.event.donationemailtemplate is not None:
                formatContext = {
                    'donation': donation,
                    'donor': donation.donor,
                    'event': donation.event,
                    'prizes': viewutil.get_donation_prize_info(donation),
                }
                post_office.mail.send(
                    recipients=[donation.donor.email],
                    sender=donation.event.donationemailsender,
                    template=donation.event.donationemailtemplate,
                    context=formatContext,
                )
            eventutil.post_donation_to_postbacks(donation)

        elif donation.transactionstate == 'CANCELLED':
            # eventually we may want to send out e-mail for some of the possible cases
            # such as payment reversal due to double-transactions (this has happened before)
            paypalutil.log_ipn(ipnObj, 'Cancelled/reversed payment')

    except Exception as inst:
        # just to make sure we have a record of it somewhere
        logging.error('ERROR IN IPN RESPONSE, FIX IT')
        if ipnObj:
            paypalutil.log_ipn(
                ipnObj,
                '{0} \n {1}. POST data : {2}'.format(
                    inst, traceback.format_exc(), request.POST
                ),
            )
        else:
            viewutil.tracker_log(
                'paypal',
                'IPN creation failed: {0} \n {1}. POST data : {2}'.format(
                    inst, traceback.format_exc(), request.POST
                ),
            )

        raise inst

    return HttpResponse('OKAY')
