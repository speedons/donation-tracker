import datetime
import locale
import urllib.parse
import tracker.viewutil

from django import template
from django.urls import reverse
from django.utils.html import conditional_escape, format_html
from django.utils.safestring import mark_safe
from djmoney.money import Money


register = template.Library()


@register.filter
def forumfilter(value, autoescape=None):
    if autoescape:
        esc = conditional_escape
    else:

        def esc(x):
            return x

    return mark_safe(esc(value).replace('\n', '<br />'))


forumfilter.is_safe = True
forumfilter.needs_autoescape = True


@register.filter
def money(value):
    locale.setlocale(locale.LC_ALL, '')
    try:
        if not value:
            return locale.currency(0.0)
        return locale.currency(value, symbol=True, grouping=True)
    except ValueError:
        locale.setlocale(locale.LC_MONETARY, 'en_US.utf8')
        if not value:
            return locale.currency(0.0)
        return locale.currency(value, symbol=True, grouping=True)


money.is_safe = True


@register.filter('abs')
def filabs(value, arg):
    try:
        return abs(int(value) - int(arg))
    except ValueError:
        raise template.TemplateSyntaxError('abs requires integer arguments')


@register.filter('mod')
def filmod(value, arg):
    try:
        return int(value) % int(arg)
    except ValueError:
        raise template.TemplateSyntaxError('mod requires integer arguments')


@register.filter('negate')
def negate(value):
    return not value


@register.simple_tag
def admin_url(obj):
    return viewutil.admin_url(obj)


@register.simple_tag(takes_context=True)
def standardform(
    context, form, formid='formid', submittext='Submit', action=None, showrequired=True
):
    return template.loader.render_to_string(
        'standardform.html',
        {
            'form': form,
            'formid': formid,
            'submittext': submittext,
            action: action,
            'csrf_token': context.get('csrf_token', None),
            'showrequired': showrequired,
        },
    )


@register.simple_tag(takes_context=True)
def form_innards(context, form, showrequired=True):
    return template.loader.render_to_string(
        'form_innards.html',
        {
            'form': form,
            'showrequired': showrequired,
            'csrf_token': context.get('csrf_token', None),
        },
    )


@register.simple_tag
def event_money(event, amount):
    return Money(amount or '0.00', event.paypalcurrency)


@register.simple_tag
def event_url(event, *args, **kwargs):
    return reverse(*args, kwargs={**kwargs, 'event': event.short})
