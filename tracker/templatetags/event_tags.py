from django import template
from django.urls import reverse
from djmoney.money import Money

register = template.Library()


@register.simple_tag
def event_money(event, amount):
    return Money(amount or '0.00', event.paypalcurrency)


@register.simple_tag
def event_url(event, *args, **kwargs):
    return reverse(*args, kwargs={**kwargs, 'event': event.short})
