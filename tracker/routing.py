from django.urls import path

from . import consumers


def channel_six(consumer):
    as_asgi = getattr(consumer, 'as_asgi', None)
    if callable(as_asgi):
        return as_asgi()
    else:
        return consumer


websocket_urlpatterns = [
    path('donations/', channel_six(consumers.DonationConsumer)),
    path('ping/', channel_six(consumers.PingConsumer)),
    path('celery/', channel_six(consumers.CeleryConsumer)),
]
