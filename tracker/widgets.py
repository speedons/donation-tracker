from django import forms
from django.utils.html import format_html


class MegaFilterWidget(forms.widgets.Widget):
    def __init__(self, model, **kwargs):
        self.model = model
        super(MegaFilterWidget, self).__init__(**kwargs)

    def value_from_datadict(self, data, files, name):
        if name in data and data[name] and data[name] != 'None':
            return int(data[name])
        else:
            return None

    def render(self, name, value, attrs=None, renderer=None):
        return format_html(
            """
            <div class="mf_widget mf_model_{0}">
                <input id="id_{1}" name="{1}" class="mf_selection" type="hidden" value="{2}" />

                <div class="form-group">
                    <input class="form-control mf_filter" type="text" />
                </div>

                <div class="form-group">
                    <select size="4" class="form-control mf_selectbox"></select>
                </div>

                <span class="mf_description" />
            </div>
            """,
            self.model,
            name,
            value,
        )


class NumberInput(forms.widgets.Input):
    def __init__(self, attrs=None):
        self.input_type = 'number'
        super(NumberInput, self).__init__(attrs)


class ReadOnlyWidget(forms.widgets.Widget):
    def render(self, _, value, attrs={}):
        return value
