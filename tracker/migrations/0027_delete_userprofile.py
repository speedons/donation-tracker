# Generated by Django 2.2.16 on 2021-02-13 22:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tracker', '0026_remove_speedrun_giantbomb_id'),
    ]

    operations = [
        migrations.DeleteModel(
            name='UserProfile',
        ),
    ]
