# Generated by Django 2.2.16 on 2020-10-19 23:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tracker', '0014_merge_20201002_2118'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='prize',
            name='imagefile',
        ),
    ]
