from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import (
    PasswordResetConfirmView,
    PasswordResetCompleteView,
    LoginView,
    LogoutView,
    PasswordResetView,
    PasswordResetDoneView,
    PasswordChangeView,
    PasswordChangeDoneView,
)
from django.urls import include, reverse_lazy, path

from tracker.ui import urls as ui_urls
from tracker.views import public, api, donateviews, user, auth

app_name = 'tracker'

api_patterns = ([
    path('', api.index, name='index'),
    path('add/', api.add, name='add'),
    path('command/', api.command, name='command'),
    path('delete/', api.delete, name='delete'),
    path('edit/', api.edit, name='edit'),
    path('me/', api.me, name='me'),
    path('search/', api.search, name='search'),

    path('ads/<int:event>/', api.ads, name='ads'),
    path('interstitial/', api.interstitial, name='interstitial'),
    path('interviews/<int:event>/', api.interviews),
    path('hosts/<int:event>/', api.hosts),
], 'api')

event_patterns = ([
    path('', public.index, name='index'),
    path('bids/', public.bidindex, name='bidindex'),
    path('bids/<int:pk>/', public.bid_detail, name='bid'),
    path('donate/', donateviews.donate, name='donate'),
    path('donate/return/', donateviews.paypal_return, name='paypal_return'),
    path('donate/cancel/', donateviews.paypal_cancel, name='paypal_cancel'),
    path('donations/', public.donationindex, name='donationindex'),
    path('donations/<int:pk>/', public.donation_detail, name='donation'),
    path('donors/', public.donorindex, name='donorindex'),
    path('donors/<int:pk>/', public.donor_detail, name='donor'),
    path('prizes/', public.prizeindex, name='prizeindex'),
    path('prizes/<int:pk>/', public.prize_detail, name='prize'),
    path('runs/', public.runindex, name='runindex'),
    path('runs/<int:pk>/', public.run_detail, name='run'),
], 'event')

urlpatterns = [
    path('', public.eventlist, name='index'),
    path('ui/', include(ui_urls, namespace='ui')),
    path('i18n/', include('django.conf.urls.i18n')),
    path('paypal/ipn/', donateviews.ipn, name='ipn'),

    path('user/', user.user_index, name='user_index'),
    path('user/user_prize/<int:prize>', user.user_prize, name='user_prize'),
    path('user/prize_winner/<int:prize_win>', user.prize_winner, name='prize_winner',),
    path('user/submit_prize/<slug:event>', user.submit_prize, name='submit_prize'),
    path('user/register/', auth.register, name='register'),
    path(
        'user/confirm_registration/<uidb64>/<token>/',
        auth.confirm_registration,
        name='confirm_registration',
    ),
    # all urls below are served by standard Django views
    path(
        'user/login/',
        LoginView.as_view(template_name='tracker/login.html'),
        name='login',
    ),
    path('user/logout/', LogoutView.as_view(next_page='tracker:login'), name='logout',),
    path(
        'user/password_reset/',
        PasswordResetView.as_view(
            template_name='tracker/password_reset.html',
            email_template_name='tracker/email/password_reset.txt',
            html_email_template_name='tracker/email/password_reset.html',
            success_url=reverse_lazy('tracker:password_reset_done'),
        ),
        name='password_reset',
    ),
    path(
        'user/password_reset_done/',
        PasswordResetDoneView.as_view(template_name='tracker/password_reset_done.html'),
        name='password_reset_done',
    ),
    path(
        'user/password_reset_confirm/<uidb64>/<token>/',
        PasswordResetConfirmView.as_view(
            template_name='tracker/password_reset_confirm.html',
            success_url=reverse_lazy('tracker:password_reset_complete'),
        ),
        name='password_reset_confirm',
    ),
    path(
        'user/password_reset_complete/',
        PasswordResetCompleteView.as_view(
            template_name='tracker/password_reset_complete.html'
        ),
        name='password_reset_complete',
    ),
    path(
        'user/password_change/',
        PasswordChangeView.as_view(
            template_name='tracker/password_change.html',
            success_url=reverse_lazy('tracker:password_change_done'),
        ),
        name='password_change',
    ),
    path(
        'user/password_change_done/',
        PasswordChangeDoneView.as_view(
            template_name='tracker/password_change_done.html'
        ),
        name='password_change_done',
    ),

    path('api/', include(api_patterns, namespace='api')),
    path('<slug:event>/', include(event_patterns, namespace='event')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
