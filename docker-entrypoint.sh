#!/usr/bin/env bash
set -e

python manage.py compilemessages
python manage.py collectstatic --no-input
python manage.py migrate --no-input

if [ -n "$EMAIL_WORKER" ]; then
  printenv > /etc/environment

  echo "* * * * * root (cd /usr/src/app; /usr/local/bin/python manage.py send_queued_mail --processes=1 >> /data/logs/send_queued_mail.log 2>&1)" > /etc/cron.d/send_queued_mail
  echo "0 0 * * * root (cd /usr/src/app; /usr/local/bin/python manage.py cleanup_mail --days=30 --delete-attachments >> /data/logs/cleanup_mail.log 2>&1)" > /etc/cron.d/cleanup_mail
fi

exec "$@"
