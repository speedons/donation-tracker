declare module '*.png';
declare module '*.jpg';
declare module '*.gif';
declare module '*.svg';

declare module '*.mod.css' {
  const styles: {
    [className: string]: string;
  };

  export default styles;
}

declare module "*.vue" {
  import { Component } from "vue";

  const component: Component;

  export default component;
}
