import { useMemo } from "react";

export function useCurrencyFormat(currency: string): Intl.NumberFormat {
  return useMemo(
    () =>
      new Intl.NumberFormat(undefined, {
        style: "currency",
        currency,
      }),
    [currency]
  );
}
