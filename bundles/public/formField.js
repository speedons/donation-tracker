import React from 'react';
import PropTypes from 'prop-types';

class FormField extends React.PureComponent {
  static defaultProps = {
    value: '',
  };

  static propTypes = {
    name: PropTypes.string.isRequired,
    type: PropTypes.string,
    modify: PropTypes.func.isRequired,
    value: PropTypes.any,
  };

  render() {
    const { name, type, value } = this.props;
    const { onChange_ } = this;
    return <input name={name} type={type} value={value} onChange={onChange_} placeholder={name} />;
  }

  onChange_ = e => {
    let {
      target: { value },
    } = e;

    if (this.props.type === "number") {
      const parsedValue = Number.parseFloat(value);

      value = Number.isFinite(parsedValue)
        ? parsedValue
        : "None";
    }

    this.props.modify(this.props.name, value);
  };
}

export default FormField;
