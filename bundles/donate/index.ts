import { createApp } from 'vue';
import { createI18n } from 'vue-i18n';

import App from './App.vue';

function bootstrap() {
  const { props } = JSON.parse(
    String(document.getElementById('__APP_DATA__')?.textContent),
  );

  const app = createApp(App, props);

  const i18n = createI18n({
    fallbackLocale: "en",
    locale: props.locale,
    messages: {
      en: require("./locales/en.json"),
      fr: require("./locales/fr.json"),
    },
  });

  const currencyFormat = new Intl.NumberFormat(undefined, {
    currency: props.event.paypal_currency,
    style: "currency",
  });

  app.use(i18n);

  app.config.globalProperties.currencyFormat = currencyFormat;

  app.directive("currency", (el, binding) => {
    el.innerText = currencyFormat
      .formatToParts(binding.value)
      .map((part) => part.type === "nan" ? "--.-" : part.value)
      .join("");
  });

  app.mount('#app');
}

bootstrap();
