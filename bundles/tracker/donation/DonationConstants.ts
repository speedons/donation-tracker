export const AMOUNT_PRESETS = [25, 50, 75, 100, 250, 500];

export const MAX_BIDS_PER_DONATION = 10;
export const BID_MINIMUM_AMOUNT = 1.0;
