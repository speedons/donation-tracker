import * as React from 'react';
import { Route, RouteComponentProps, Switch } from 'react-router-dom';

import Donate from '../../donation/components/Donate';
import DonateInitializer from '../../donation/components/DonateInitializer';
import { Routes } from '../../router/RouterUtils';
import NotFound from '../../router/components/NotFound';

const EventRouter = (props: any) => {
  // TODO: type this better when DonateInitializer doesn't need page-load props
  const { eventId } = props;

  return (
    <Switch>
      <Route exact path={Routes.EVENT_DONATE(eventId)}>
        {({ match }: RouteComponentProps<{ eventId: string }>) => (
          <React.Fragment>
            <DonateInitializer {...props} />
            <Donate eventId={eventId} />
          </React.Fragment>
        )}
      </Route>
      <NotFound />
    </Switch>
  );
};

export default EventRouter;
