FROM node:12 AS client

WORKDIR /usr/src/app

COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile

COPY .browserslistrc babel.config.js postcss.config.js webpack.config.js ./
COPY bundles bundles

RUN yarn build

FROM python:3.10-bullseye

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

RUN apt-get update && \
  apt-get install --no-install-recommends -y cron gcc gettext && \
  rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

RUN pip install --upgrade pip && \
  pip install django~=2.2 channels_redis django-cache-url dj-database-url django-redis envparse gunicorn psycopg2-binary==2.8.6 twisted==20.3 uvicorn==0.15.0 uvicorn[standard]

RUN django-admin startproject tracker_project ./

COPY . tracker_project/tracker_app/
COPY --from=client /usr/src/app/tracker tracker_project/tracker_app/tracker

RUN cd tracker_project && \
  pip install -e tracker_app

COPY .docker tracker_project/

COPY docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh

VOLUME /data/logs /data/media /data/static
EXPOSE 8000

ENTRYPOINT [ "/docker-entrypoint.sh" ]
CMD [ "gunicorn", "-b", "0.0.0.0:8000", "-w", "4", "-k", "uvicorn.workers.UvicornWorker", "tracker_project.asgi:application" ]
